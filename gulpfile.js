var elixir = require('laravel-elixir');

elixir.config.sourcemaps = true;

elixir(function (mix) {
    mix.sass('app.scss')
        .scripts([
            // 'vendor/jquery.js',
            'vendor/modernizr.js',
            'vendor/foundation.min.js',
            'vendor/isotope.pkgd.min.js',
            'vendor/slick.min.js',
            'vendor/jquery.nicescroll.min.js',
            'vendor/jquery.visible.min.js',
            'vendor/waitForImages.js',
            'script.js'
        ],'public/js/all.js', 'resources/assets/js/');
});
