@extends('index')

@section('content')
    <div class="recipe-gallery bgColor-wheat">

        <section class="section-1 padding-tb-50 s-o-padding-t-95 s-o-padding-b-30 m-o-padding-t-130 m-o-padding-b-50">
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="default-font-family default-title-style no-margin">
                            Our Recipes
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="large-12 small-12 medium-12 columns">
                    <div class="recipe-grid">
                        <ul class="recipe-menu small-block-grid-1 medium-block-grid-2 large-block-grid-4">

                            @foreach($recipes as $key => $value)
                                <li class="recipe-items">
                                    <div class="box-holder">
                                        <a class="thumb" href="{{route('recipes',[$key])}}">
                                            <img class="recipe-img" src="../img/gallery/{{$value["recipe_image"]}}"
                                                 alt="">
                                            <span class="thumb-overlay"></span>
                                        </a>

                                        <div class="grid-item">
                                            <h4 class="no-margin font-lato font-weight-600">{{$value["recipe_name"]}}</h4>
                                            <!-- <span>Interior</span> -->
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()


