@extends('index')

@section('content')
    <div class="product-page">
        <section
                class="section-1 bgColor-wheat padding-tb-50 s-o-padding-t-100 s-o-padding-b-30 m-o-padding-t-90 m-o-padding-b-50">
            <div class="row">
                <div class="columns small-12">
                    <div class="page-title large-text-center small-text-left default-font-family">
                        Jaggery Product (Lump)
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="columns large-8 small-12 medium-8">

                    <div class="product-slider">

                        <div class="slide">
                            <div class="image">
                                <img src="../img/product/pro1.jpg">
                            </div>
                        </div>
                    </div>

                    <div class="product-title margin-tb-20">
                        <h4 class="darkBrown-color font-weight-600 custom-lh">
                            Jaggery Lump
                        </h4>
                    </div>

                    <div class="product-desc">
                        <p class="darkBrown-color">
                            Organic jaggery lump is produced without use of any chemicals like sulphur dioxide, which is
                            generally used as bleaching agents so that the jaggery has a lighter colour. Were refined
                            sugar loses much of its nutritional content during production, jaggery contain trace
                            minerals and vitamins, making it truly healthier alternative sweetener.
                        </p>
                    </div>
                </div>
                <div class="columns large-4 small-12 medium-4">
                    <!-- <div class="section-header margin-b-only">
                        <h2 class="darkBrown-color no-margin font-italics font-serif font-weight-600">
                            Points
                        </h2>
                    </div> -->

                    <div class="points-list">
                        <table>
                            <tbody>
                            @foreach($points as $index => $point)
                                <tr>
                                    <td>
                                        <i class="fa fa-arrow-circle-right"></i>

                                    </td>
                                    <td>
                                        <span class="margin-l-15">{{$point}}</span>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection()