var _route = '';

$(document).ready(function () {

    _route = $('meta[name=_route]').attr('content');

    $(document).foundation();

    $('.product-slider').slick({
        arrows: true,
        autoplay: true,
        pauseOnHover: false,
        autoplaySpeed: 8000,
        speed: 300,
        cssEase: 'ease',
        fade: true,
        infinite: true,
        lazyLoad: "ondemand",
        prevArrow: '<span class="nav-prev"><i class="fa fa-angle-left"></i></span>',
        nextArrow: '<span class="nav-next"><i class="fa fa-angle-right"></i></span>'
    });


    /* HEADER ANIMATION */
    if ($('.pawak-header').is(":visible")) {
        $(window).on("scroll", function () {
            if ($(window).scrollTop() > 100) {
                $(".pawak-header").addClass("stickyHeader");
            }
            else {
                $(".pawak-header").removeClass("stickyHeader");
            }
        })
    }


    $(document).click(function (e) {
        if ($(e.target).is('#products-menu *')) {
            $('.navMenu-items').find('.product-menu-lists').addClass('slide-menu');
        }
        else {
            $('.navMenu-items').find('.product-menu-lists').removeClass('slide-menu');
        }
    });

    $(document).on('mouseover',function (e) {
        if ($(e.target).is('#products-menu *')) {
            $('.navMenu-items').find('.product-menu-lists').addClass('slide-menu');
        }
        else {
            $('.navMenu-items').find('.product-menu-lists').removeClass('slide-menu');
        }
    });

    $.each($('.height-equalizer-wrapper'), function () {
        
        var totalChildEqualizers = $(this).find('.height-equalizer');
        var totalChildrenHeightArr = new Array();
        var indexOfMax;
        $.each(totalChildEqualizers, function () {
            totalChildrenHeightArr.push($(this).outerHeight());
        });
        indexOfMax = totalChildrenHeightArr.indexOf(Math.max.apply(Math, totalChildrenHeightArr));
        totalChildEqualizers.outerHeight($(totalChildEqualizers[indexOfMax]).outerHeight());
    });


});









