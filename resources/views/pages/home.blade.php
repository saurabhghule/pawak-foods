@extends('index')

@section('content')

    <div class="home-page">
        <section class="section-1 banner-img">

            <div class="banner parent-table">
                {{--<img src="../img/paawak-banner-image.jpg">--}}
                <div class="overlay"></div>
                <div class="banner-tagline child-table-cell">

                    <div class="row">
                        <div class="columns large-12 small-12 medium-12">
                            <h1 class="whiteColor no-margin custom-lh default-font-family">
                                We have best quality <br/>
                                fresh Jaggery cubes
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-2 padding-tb-50 s-o-padding-tb-40 m-o-padding-tb-50">
            <div class="row">
                <div class="columns large-8 medium-12 small-12">
                    <div class="welcome-section">
                        <h2 class="no-margin default-title-style default-font-family small-text-center-only">Welcome to Paawak Foods!</h2>

                        <p class="no-margin small-text-center-only">
                            Paawak Foods is a fast growing company engaged in agro products. 
                            Since inception the company’s core business has been jaggery trading, 
                            which has expanded prodigiously over the years. The quality of our jaggery 
                            products is optimal as they are procured from the best sources. 
                            We process them under hygienic conditions by adopting quality methods. 
                            We conduct strict quality check tests before delivering the products to 
                            our clients to ensure that no sub-standard product is delivered to the client.
                        </p>
                    </div>
                </div>

                <div class="columns large-4 medium-12 small-12 text-center">
                    <div class="button-content">
                        <div class="wrapper">
                            <a href="{{route('about')}}"
                               class="bgColor-darkPink about-btn whiteColor text-center">ABOUT US</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-3 padding-tb-50 bgColor-wheat s-o-padding-tb-40 m-o-padding-tb-50 default-font-color">
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="no-margin default-font-family default-title-style">
                            Our Products
                        </h2>
                    </div>
                </div>
                <div class="columns small-12">
                    <div class="description">
                        We have carved a niche for ourselves in the arena of manufacturing a comprehensive range of natural jaggery products. <br/><br/>
                        As per the research, jaggery is not given the first predilection as natural sweetener because of the inconsistently 
                        erratic size of jaggery cubes. Hence, to eradicate the problem of inconsistency of size we have introduced smaller 
                        and natural jaggery cubes. <br/><br/>
                        ‘Purifying Yourself’ is the mantra of our ‘Homa Therapy Agnihotra jaggery powder. 
                        The central idea of Homa Therapy is, you heal the atmosphere and the healed atmosphere heals you. 
                        Paawak natural jaggery powder is made from specially grown sugar cane which is cultivated as per 
                        Homa and Agnihotra process. <br/><br/>
                        Our natural jaggery is available in two variants:
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="columns large-6 small-12 medium-6 text-center">
                    <div class="product-display-section">
                        <a href="{{route('lump_products')}}" class="inline-block link-hover-effect">
                            <img src="../img/home/o_j_l.jpg">

                            <h4 class="font-weight-600 margin-t20 no-margin-b default-font-color s-o-margin-b-30">NATURAL JAGGERY CUBES</h4>
                        </a>
                    </div>
                </div>

                <div class="columns large-6 small-12 medium-6 text-center">
                    <div class="product-display-section">
                        <a href="{{route('powdered_products')}}" class="inline-block link-hover-effect">
                            <img src="../img/home/o_j_p.jpg">

                            <h4 class="font-weight-600 margin-t20 no-margin-b default-font-color">NATURAL JAGGERY
                                POWDER</h4>
                        </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-4 padding-tb-50 s-o-padding-tb-30 m-o-padding-tb-50">
            <div class="row">
                <div class="columns small-12 medium-12 large-12 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="no-margin default-title-style default-font-family">
                            Recipes made from Paawak Jaggery
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="columns large-4 medium-6 small-12">
                    <a href="{{route('recipes',['1'])}}" class="recipe-img margin-b-25">
                        <img src="../img/gallery/20.jpg">

                        <div class="recipe-overlay"></div>
                        <div class="bgColor-wheat grid-item">
                            <h4 class="no-margin font-lato font-weight-600">Sweetend Rice Flakes</h4>
                        </div>
                    </a>
                </div>

                <div class="columns large-4 medium-6 small-12">
                    <a href="{{route('recipes',['3'])}}" class="recipe-img margin-b-25">
                        <img src="../img/gallery/2.jpg">

                        <div class="recipe-overlay"></div>
                        <div class="bgColor-wheat grid-item">
                            <h4 class="no-margin font-lato font-weight-600">Ragi Dills</h4>
                        </div>
                    </a>
                </div>
                <div class="columns large-4 medium-6 small-12">
                    <a href="{{route('recipes',['6'])}}" class="recipe-img margin-b-25">
                        <img src="../img/gallery/5.jpg">

                        <div class="recipe-overlay"></div>
                        <div class="bgColor-wheat grid-item">
                            <h4 class="no-margin font-lato font-weight-600">Peanut Chikki</h4>
                        </div>
                    </a>
                </div>

                <div class="columns large-4 medium-6 small-12">
                    <a href="{{route('recipes',['17'])}}" class="recipe-img margin-b-25">
                        <img src="../img/gallery/16.jpg">

                        <div class="recipe-overlay"></div>
                        <div class="bgColor-wheat grid-item">
                            <h4 class="no-margin font-lato font-weight-600">Sweet Cubes</h4>
                        </div>
                    </a>
                </div>
                <div class="columns large-4 medium-6 small-12">
                    <a href="{{route('recipes',['10'])}}" class="recipe-img margin-b-25">
                        <img src="../img/gallery/9.jpg">

                        <div class="recipe-overlay"></div>
                        <div class="bgColor-wheat grid-item">
                            <h4 class="no-margin font-lato font-weight-600">Ninava</h4>
                        </div>
                    </a>
                </div>
                <div class="columns large-4 medium-6 small-12">
                    <a href="{{route('recipes',['14'])}}" class="recipe-img margin-b-25">
                        <img src="../img/gallery/13.jpg">

                        <div class="recipe-overlay"></div>
                        <div class="bgColor-wheat grid-item">
                            <h4 class="no-margin font-lato font-weight-600">Rice Flakes Toffee</h4>
                        </div>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="columns small-12 text-center">
                    <div class="cta-btn">
                        <a href="{{route('recipe-corner')}}"
                           class="bgColor-darkPink about-btn whiteColor text-center view-recipe-button">VIEW MORE
                       </a>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-5 padding-tb-50 bgColor-wheat s-o-padding-tb-40 m-o-padding-tb-50">
            <div class="row">
                <div class="columns small-12">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="no-margin default-title-style default-font-family">
                            What Client Say About Us
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row height-equalizer-wrapper">
                <div class="columns large-4 medium-12 small-12">
                    <blockquote class="blockquote-panel">
                        <p class="height-equalizer"><q>
                                <span>“</span>
                                Paawak Jaggery is the most naturally made and tastes very good. 
                                Its original sugarcane taste is what makes it very distinct. 
                                In the world where there is food adulteration in almost every thing, 
                                it's good to know that brands like Paawak Jaggery are making such 
                                healthy and delicious products.</q></p>

                        <cite>Dinesh Solanki</cite>
                    </blockquote>
                </div>
                <div class="columns large-4 medium-12 small-12">
                    <blockquote class="blockquote-panel">
                        <p class="height-equalizer"><q>
                                <span>“</span>
                                Paawak organic jaggery is good for our health and it prevents constipation, 
                                helps to build haemoglobin, increases immunity. Treats flu like symptoms like 
                                cough - cold, migraine. We can use Paawak organic jaggery to make laddus and mithai.</q></p>

                        <cite>Ujwala Sancheti</cite>
                    </blockquote>
                </div>
                <div class="columns large-4 medium-12 small-12">
                    <blockquote class="blockquote-panel">
                        <p class="height-equalizer"><q>
                                <span>“</span>
                                I love the brand Paawak because I use it for my kid. 
                                It suits him that's the best part. I love Paawak specially cause of the customer service. 
                                They are very prompt and efficient. Please keep up the good work.</q></p>

                        <cite>Aneesha Prakash</cite>
                    </blockquote>
                </div>
            </div>
        </section>
    </div>


@endsection