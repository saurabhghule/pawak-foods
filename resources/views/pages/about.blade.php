@extends('index')

@section('content')


    <div class="about-page">
        <section class="section-1 bgColor-wheat padding-tb-50 s-o-padding-t-95 s-o-padding-b-30 m-o-padding-t-130 m-o-padding-b-50">
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="default-font-family default-title-style">
                            About us
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="columns small-12 large-5 medium-12">
                    <div class="aboutCard">
                        <img src="/img/about/first_image.jpg" alt=""/>
                    </div>
                </div>
                <div class="columns small-12 large-7 medium-12">
                    <div class="aboutCard-content">
                        <h3 class="darkBrown-color font-weight-600 small-only-margin25">
                            Paawak Foods a natural jaggery manufacturer.
                        </h3>

                        <p class="darkBrown-color fontSize1">
                            The company has a dedicated team of professionals who acquire
                            premium quality products from its most trusted engenders,
                            and offer it to the clients in impeccable state with the
                            desired quality. The firm is engaged in supplying Jaggery
                            across various components of India.
                        </p>

                        <p class="darkBrown-color fontSize1">
                            The company believes in delivering products with main focus on:
                        </p>
                        <ul>
                            <li>Strong base and ethical values</li>
                            <li>Customer centric approach</li>
                            <li>Integrity and Innovation</li>
                            <li>Inculcating health consciousness</li>
                            <li>Creating strong brand value</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-2 bgColor-white padding-tb-50 s-o-padding-tb-30 m-o-padding-tb-50">
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="default-font-family default-title-style">
                            Our Commitments
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="columns small-12 large-6 medium-6">
                    <div class="row">
                        <div class="columns large-4">
                            <div class="product-img">
                                <img src="../img/about/pic-2.jpg">
                            </div>
                        </div>

                        <div class="columns large-8">
                            <div class="product-content">
                                <h4 class="margin-b-only darkPink-color s-o-margin-tb-15 m-o-margin-tb-20 font-weight-600">
                                    OUR VISION
                                </h4>

                                <h5 class="font-lato darkBrown-color s-o-margin-b-30">
                                    To enhance quality of jaggery manufacturing through improved
                                    post-harvest processes, technology, homa therapy, energy
                                    field of Agnihotra management and value additions.
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="columns small-12 large-6 medium-6">
                    <div class="row">
                        <div class="columns large-4">
                            <div class="product-img">
                                <img src="../img/about/pic-3.jpg">
                            </div>
                        </div>

                        <div class="columns large-8">
                            <div class="product-content">
                                <h4 class="margin-b-only darkPink-color s-o-margin-tb-15 m-o-margin-tb-20 font-weight-600">
                                    OUR MISSION
                                </h4>

                                <h5 class="font-lato">
                                    To improve productivity, quality and storability of jaggery
                                    and demonstrate the improved post-harvest processes and
                                    equipment/gadgets to the users for adoption.
                                </h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-3 padding-tb-50 s-o-padding-tb-30 m-o-padding-tb-50">
            <div class="bg-img">
                <div class="overlay"></div>
            </div>
            <div class="row">
                <div class="columns small-12 medium-12 large-6 large-centered">
                    <div class="section-header margin-b-25 large-text-center small-text-left">
                        <h2 class="whiteColor default-font-family default-title-style">
                            Our Team
                        </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="large-12 columns small-12 medium-12">
                    <div class="section-content">
                        <p class="section-content whiteColor margin-b-30">
                            We have a highly adroit and experienced team of professionals
                            from the field of Management, Food processing & Marketing, who
                            consistently work strenuously to maintain the quality of our
                            agro products. They are the most precious assets & the backbone
                            of our company and their main motto is to provide utmost level
                            of gratification to our clients.
                        </p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="columns large-3 small-12 medium-3">
                    <div class="team-card">
                        <div class="founder-img">
                            <img src="../img/about/team/t3.jpg" alt=""/>

                            <div class="img-overlay"></div>
                        </div>
                        <div class="founder-content">
                            <h4 class="whiteColor custom-margin">KAUSHAL DONGRE</h4>

                            <p class="whiteColor s-o-margin-b-30">
                                In a startup, it is important that every person on the core team makes a valuable contribution, 
                                and Kaushal did so for Paawak Foods. Kaushal played a major role in developing the 
                                sales pitch, helped create new and sustainable business relations and also helped 
                                establish Paawak Foods brand presence in a nascent market.
                            </p>
                        </div>
                        <div class="social-icons"></div>
                    </div>
                </div>
                
                <div class="columns large-3 small-12 medium-3">
                    <div class="team-card">
                        <div class="founder-img">
                            <img src="../img/about/team/t2.jpg" alt=""/>

                            <div class="img-overlay"></div>
                        </div>
                        <div class="founder-content">
                            <h4 class="whiteColor custom-margin">Dr. NILESH S AMRITKAR</h4>

                            <p class="whiteColor s-o-margin-b-30">
                                Dr. Nilesh Amritkar has done his PhD in Microbiology from UDCT. 
                                With a scholastic background and mandate to empower 
                                Indian Industry Professionals, Govt. Regulators & Academia with knowledge & 
                                skills drive him to organize training programs, seminars & workshops. 
                                Due to scientific fervor, entrepreneurship, nationalistic spirit, 
                                vision and social commitment, Dr. Amritkar has been delivering several 
                                responsibilities while occupying prestigious positions.
                            </p>
                        </div>
                        <div class="social-icons"></div>
                    </div>
                </div>
                <div class="columns large-3 small-12 medium-3">
                    <div class="team-card">
                        <div class="founder-img">
                            <img src="../img/about/team/t1.jpg" alt=""/>

                            <div class="img-overlay"></div>
                        </div>
                        <div class="founder-content">
                            <h4 class="whiteColor custom-margin">Mr. M. M. CHITALE</h4>

                            <p class="whiteColor s-o-margin-b-30">
                                Mr. Chitale has done his B.Sc. (Tech) in Food Technology. 
                                He has a 40 years of diversified   experience in R&D Product Development, 
                                Quality Control,  Production,  Projects, Factory & General Management disciplines. 
                                Mr. Chitale has an exposure in Plant and Machinery sourcing, Negotiations & Purchase, 
                                Technological Training in the factories of foreign collaborator.
                            </p>
                        </div>
                        <div class="social-icons"></div>
                    </div>
                </div>
                <div class="columns large-3 small-12 medium-3">
                    <div class="team-card">
                        <div class="founder-img">
                            <img src="../img/about/team/t4.jpg" alt=""/>

                            <div class="img-overlay"></div>
                        </div>
                        <div class="founder-content">
                            <h4 class="whiteColor custom-margin">MAHESH DURADI</h4>

                            <p class="whiteColor">
                                He started Homa Organic farming since 2008 with 28 acres of land where he grows sugarcane, 
                                turmeric and maize without using any synthetic fertilizers & pesticides. 
                                With his expertise he manufactures Paawak Natural Jaggery Powder.
                            </p>
                        </div>
                        <div class="social-icons"></div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-4 bgColor-wheat padding-tb-50 s-o-padding-tb-40 m-o-padding-tb-50">
            <div class="row">
                <div class="columns small-12 large-8 medium-8">
                    <div class="product-content">
                        <h2 class="default-font-family default-title-style margin-b-only">
                            <i class="default-font-family default-title-style">
                                Why Paawak?
                            </i>
                        </h2>

                        <div class="table-content">
                            <table>
                                <tbody>
                                <tr>
                                    <td>
                                        <i class="fa fa-check"></i>
                                    </td>
                                    <td>
                                        Our jaggery is a good source of minerals like potassium and calcium
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-check"></i>
                                    </td>
                                    <td>
                                        The potassium present in our jaggery maintain the acid balance in the body
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-check"></i>
                                    </td>
                                    <td>
                                        Fructose present in our jaggery helps in slow release of energy for body
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <i class="fa fa-check"></i>
                                    </td>
                                    <td>
                                        Our jaggery is considered as the best base material for the preparation of
                                        medicine
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
                <div class="columns small-12 large-4 medium-4">
                    <div class="product-img">
                        <div class="content">
                            <img src="../img/about/why.png">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="section-5 padding-tb-100 s-o-padding-tb-40 m-o-padding-tb-50">
            <div class="bg-img"><div class="overlay"></div></div>
            <div class="row">
                <div class="columns large-8 small-12 medium-12 large-centered">
                    <h1 class="no-margin whiteColor text-center font-italics font-serif font-weight-600">
                        Paawak Jaggery is delicious, hygienic, and without harmful chemicals
                    </h1>
                </div>
            </div>
        </section>
    </div>

@endsection